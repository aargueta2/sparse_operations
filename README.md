These are the operations supported by this repository:

1) Fused affine transformation + Softmax + top-k
2) Sparse n-hot lookup

Run:
mkdir bin
make 

The following binaries will be in the bin/ directory
1) cudaTensorCublasComp This binary was used to run the Softmax + top-k code
2) firstLayer Binary used for the sparse n-hot lookup experiments


