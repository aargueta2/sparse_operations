/*
 * Header containing the methods from the following paper:
 * https://arxiv.org/abs/1805.02867
 * This code comes from the authors in the paper
 */
#include <algorithm>
#include <cassert>
#include <cfloat>
#include <cub/cub.cuh>
#include <curand.h>
#include <iomanip>
#include <iostream>
#include <limits>
#include <math.h>
#include <stdio.h>
#include <string>
#include <tuple>
#include <vector>

#define CUDA_CHECK(callstr) {cudaError_t error_code = callstr; if (error_code != cudaSuccess) { std::cerr << "CUDA error " << error_code << " at " << __FILE__ << ":" << __LINE__; assert(0); } }
#define CURAND_CHECK(callstr) {curandStatus_t error_code = callstr; if (error_code != CURAND_STATUS_SUCCESS) { std::cerr << "cuRAND error " << error_code << " at " << __FILE__ << ":" << __LINE__; assert(0); } }

#define CURAND_CHECK(callstr) {curandStatus_t error_code = callstr; if (error_code != CURAND_STATUS_SUCCESS) { std::cerr << "cuRAND error " << error_code << " at " << __FILE__ << ":" << __LINE__; assert(0); } }

//This is the maximum number of elements each thread must hold/handle
const int MAX_K=50;

//Structure used to store the top-k elements in one single thread
template<int MAX_K>
struct TopK
{
    int p[MAX_K];
    float u[MAX_K];

    __device__ __forceinline__ void insert(float elem, int elem_id)
    {
        if (elem > u[MAX_K-1])
        {
            u[MAX_K-1] = elem;
            p[MAX_K-1] = elem_id;
        }
        for(int k = MAX_K - 2; k >= 0; --k)
        {
            if (u[k+1] > u[k])
            {
                float u2 = u[k];
                int p2 = p[k];
                u[k] = u[k+1];
                p[k] = p[k+1];
                u[k+1] = u2;
                p[k+1] = p2;
            }
        }
    }
};

template<int MAX_K>
struct TopKD
{
    float d;
    TopK<MAX_K> topk;
};

//Operation used to merge all top-k elements in the thread block
template<int MAX_K>
__device__ __forceinline__ TopK<MAX_K> reduce_topk_op(const TopK<MAX_K>& a, const TopK<MAX_K>& b)
{
    TopK<MAX_K> res = a;
    for(int i = 0; i < MAX_K; ++i)
        res.insert(b.u[i], b.p[i]);
    return res;
}

template<int MAX_K>
__device__ __forceinline__ TopKD<MAX_K> reduce_topk_d_op(const TopKD<MAX_K>& a, const TopKD<MAX_K>& b)
{
    TopKD<MAX_K> res;
    res.d = a.d + b.d;
    res.topk = reduce_topk_op(a.topk, b.topk);
    return res;
}

template<int MAX_K>
struct TopK_Compressed
{
    unsigned long long int p[MAX_K];

    __device__ __forceinline__ void insert(unsigned long long int compressed)
    {
        if (compressed > p[MAX_K-1])
        {
            p[MAX_K-1] = compressed;
        }
        for(int k = MAX_K - 2; k >= 0; --k)
        {
            if (p[k+1] > p[k])
            {
                int p2 = p[k];
                p[k] = p[k+1];
                p[k+1] = p2;
            }
        }
    }
};

template<int MAX_K>
struct TopKD_Compressed
{
    float d;
    TopK_Compressed<MAX_K> topk;
};

template<int MAX_K>
__device__ __forceinline__ TopK_Compressed<MAX_K> reduce_topk_op(const TopK_Compressed<MAX_K>& a, const TopK_Compressed<MAX_K>& b)
{
    TopK_Compressed<MAX_K> res = a;
    for(int i = 0; i < MAX_K; ++i)
        res.insert(b.p[i]);
    return res;
}

template<int MAX_K>
__device__ __forceinline__ TopKD_Compressed<MAX_K> reduce_topk_d_op_compressed(const TopKD_Compressed<MAX_K>& a, const TopKD_Compressed<MAX_K>& b)
{
    TopKD_Compressed<MAX_K> res;
    res.d = a.d + b.d;
    res.topk = reduce_topk_op(a.topk, b.topk);
    return res;
}



/// This method can return the execution time in ms
float benchmark_softmax_topk(int V, int K, int batch_size);

// This method is used to get the top-k elements from a set of values
// stored in GPU device memory
float nvidia_softmax_topK(float **values, int **index, float **probabilities, int num_elements, int top_k, int batch_size, float * x_host);

// This method is used to get the top-k elements from a set of values
// This method computes algorithm 3 on the https://arxiv.org/pdf/1805.02867.pdf paper
float nvidia_softmax_topK_online(float **values, int **index, float **probabilities, int num_elements, int top_k, int batch_size, float * x_host);
