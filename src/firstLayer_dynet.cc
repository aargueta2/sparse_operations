#include <queue>
#include <stdio.h>
#include <cstdlib>

#include "dynet/dynet.h"
#include "dynet/cuda.h"
#include <dynet/param-init.h>
#include "dynet/training.h"
#include "dynet/lstm.h"
#include <dynet/io.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/device_ptr.h>
#include <vector>
#include <string>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

namespace po = boost::program_options;
using namespace std;
using namespace dynet;

#define BATCH_SIZE 100
#define EMBEDDINGS_SIZE 512
#define INPUT_VOCAB_SIZE 10240


LookupParameter input_lookup;

int main(int argc, char **argv) {
  po::options_description desc("*** Mini-batch beam search decoder ***");


  desc.add_options()
    ("help", "produce help message")
    ("source_file", po::value<string>()->default_value(""), "File with the source language used for training")
    ("target_file", po::value<string>()->default_value(""), "File with the target language used for training")
    ("save_model", po::value<string>()->default_value(""), "File with the name of the input/output model file")
    ("source_alphabet", po::value<string>()->default_value(""), "File with the source tokens")
    ("target_alphabet", po::value<string>()->default_value(""), "File with the target tokens")
    ("load_flag", po::value<int>()->default_value(0), "Indicates if the model needs to be loaded or not")
    ("save_flag", po::value<int>()->default_value(0), "Indicates if the model needs to be saved or not")
    ("dynet-mem", po::value<int>()->default_value(0), "Indicates the amount of dynet memory")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);



  //Initialize dynet
  dynet::initialize(argc, argv);

  ParameterCollection model;

  vector<unsigned> input_vector(BATCH_SIZE);
  for(int index = 0; index < input_vector.size(); ++index){
    int assign_val = rand() % INPUT_VOCAB_SIZE;
  }

  //Create the lookup table (This is equal to the dense matrix representation)
  input_lookup = model.add_lookup_parameters(INPUT_VOCAB_SIZE, { EMBEDDINGS_SIZE },ParameterInitUniform(-0.05,0.05),"inputlookup");
  ComputationGraph cg;
  cg.set_immediate_compute(true);
 
  cudaDeviceSynchronize();
  cudaEvent_t startOneHot;
  cudaEvent_t stopOneHot;

  cudaEventCreate(&startOneHot);
  cudaEventCreate(&stopOneHot);
  cudaEventRecord(startOneHot);

  Expression last_output_embeddings = lookup(cg, input_lookup, input_vector);

  cudaEventRecord(stopOneHot);
  cudaEventSynchronize(stopOneHot);

  float one_hot_time;
  cudaEventElapsedTime(&one_hot_time, startOneHot, stopOneHot);
  printf("one-hot lookup took %f ms\n", one_hot_time);

  cudaEventDestroy(startOneHot);
  cudaEventDestroy(stopOneHot);


  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }


  return 0;

}


