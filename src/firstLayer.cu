/* Copyright (c) 1993-2017, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <curand.h>
#include <cublas_v2.h>
#include <cstdlib>

#include <thrust/device_vector.h>
#include <cuda_runtime.h>
#include <cusparse_v2.h>
#include "cusparse.h"

#define DEBUG 0

__device__ float atomicMax(float * const address, const float value)
{
	if (* address >= value)
	{
		return *address;
	}

	int * const address_as_i = (int *)address;
	int old = * address_as_i, assumed;

	do 
	{
		assumed = old;
		if (__int_as_float(assumed) >= value)
		{
			break;
		}

		old = atomicCAS(address_as_i, assumed, __float_as_int(value));
	} while (assumed != old);

        return __int_as_float(old);
}


// Define some error checking macros.
#define cudaErrCheck(stat) { cudaErrCheck_((stat), __FILE__, __LINE__); }
void cudaErrCheck_(cudaError_t stat, const char *file, int line) {
   if (stat != cudaSuccess) {
      fprintf(stderr, "CUDA Error: %s %s %d\n", cudaGetErrorString(stat), file, line);
      exit(0);
   }
}

#define cublasErrCheck(stat) { cublasErrCheck_((stat), __FILE__, __LINE__); }
void cublasErrCheck_(cublasStatus_t stat, const char *file, int line) {
   if (stat != CUBLAS_STATUS_SUCCESS) {
      fprintf(stderr, "cuBLAS Error: %d %s %d\n", stat, file, line);
      exit(0);
   }
}

#define curandErrCheck(stat) { curandErrCheck_((stat), __FILE__, __LINE__); }
void curandErrCheck_(curandStatus_t stat, const char *file, int line) {
   if (stat != CURAND_STATUS_SUCCESS) {
      fprintf(stderr, "cuRand Error: %d %s %d\n", stat, file, line);
      exit(0);
   }
}



// Must be multiples of 16 for wmma code to work
#define MATRIX_M 100 // THIS WILL BE THE "BATCH" SIZE
#define MATRIX_N 512 // DIMENSIONS OF THE WORD VECTORS
#define MATRIX_K 10240 // VOCAB SIZE IN THIS CASE
#define NUM_SPARSE_ELEM 100 //Number of sparse elements per batch input


//Convert the single precission input to half
__global__ void convertFp32ToFp16 (half *out, float *in, int n) {
   int idx = blockDim.x * blockIdx.x + threadIdx.x;
   if (idx < n) {
      out[idx] = in[idx];
   }
}

//Convert the single precission input to half
__global__ void convertFp32ToFp16_column (half *out_row_major, half *out_col_major, float *out_col_major_float, float *in, int n, int dim_k, int dim_n) {
   int idx = blockDim.x * blockIdx.x + threadIdx.x;
   if (idx < n) {
      int row_major_row = idx / dim_n;
      int row_major_col = idx % dim_n;
      int col_major_index = row_major_col * dim_k + row_major_row;

      out_row_major[idx] = in[idx];
      out_col_major[col_major_index] = in[idx];
      out_col_major_float[col_major_index] = in[idx];
   }
}


/// This class stores the input to the affine transform
class GemmArguments {

  public:
    thrust::device_vector<half> a_fp16;
    
    thrust::device_vector<int> a_row;
    thrust::device_vector<int> a_col;
    thrust::device_vector<float> a_values;
    thrust::device_vector<half> a_values_half;

    thrust::device_vector<half> b_fp16;
    thrust::device_vector<half> b_col_fp16;

    thrust::device_vector<float> c_cublas;

    // Bucketsort parameters
    int max_val;
    int min_val;     

    GemmArguments(int a_dim, int b_dim, int c_dim) {
      a_fp16.resize(a_dim, 0);
      b_fp16.resize(b_dim, 0);
      b_col_fp16.resize(b_dim, 0);
      c_cublas.resize(c_dim, 0);

      a_row.resize((MATRIX_M + 1), 0);
      a_col.resize(a_dim, 0);
      a_values.resize(a_dim, 0);
      a_values_half.resize(a_dim, 0);

      max_val = 0;
      min_val = 0;
    }

    ~GemmArguments() {
      a_fp16.clear();
      a_fp16.shrink_to_fit();

      a_row.clear();
      a_row.shrink_to_fit();

      a_col.clear();
      a_col.shrink_to_fit();

      a_values.clear();
      a_values.shrink_to_fit();

      a_values_half.clear();
      a_values_half.shrink_to_fit();

      c_cublas.clear();
      c_cublas.shrink_to_fit();
    }
};



/// Sparse GEMM computation.
/// This implementation computes an affine transformation
/// and returns the largest element in the entire result
__global__ void FirstLayer_kernel(
  int M,
  int N,
  int K,
  float alpha,
  half const *A,
  int const *A_rows,
  int const *A_cols,
  int lda,
  half const *B,
  int ldb,
  float beta,
  float *C,
  int ldc) {

 
  int i = threadIdx.x + blockIdx.x * blockDim.x; // Obtain a row
  int j = threadIdx.y + blockIdx.y * blockDim.y; // Obtain a  column

  if (i < M && j < N) {
    float accumulator = 0;

    //Obtain the "Correct" k dimension from the csr format
    int k_dim_start = A_rows[i];
    int k_dim_end = A_rows[i+1];

    //Iterate over the modified k dimension
    for (int k = k_dim_start; k < k_dim_end; ++k) {
      float A_val = float(A[k]);
      int A_col = A_cols[k]; // Obtain the value in the k dimension to iterate
      accumulator += float(A_val) * float(B[j + A_col * ldb]);
    }

    //In this case, the C matrix should be all zeroes
    C[j + i * ldc] = alpha * accumulator + beta * C[j + i * ldc];
  }
}



/// Reference GEMM computation.
// This is the method used to compute the sparse n-hot lookup
void FirstLayerOperation(
  int M,
  int N,
  int K,
  float alpha,
  half const *A,
  int const *A_rows,
  int const *A_cols,
  int lda,
  half const *B,
  int ldb,
  float beta,
  float *C,
  int ldc) {
  
  //Block dimensions for the kernel call
  dim3 block(32, 32);
  dim3 grid(
    (M + block.x - 1) / block.x,
    (N + block.y - 1) / block.y
  );

  FirstLayer_kernel<<< grid, block >>> (
    M,
    N,
    K,
    alpha,
    A,
    A_rows,
    A_cols,
    lda,
    B,
    ldb,
    beta,
    C,
    ldc
  );

}

/****
 * This method assigns values to a column major vector
 * All odd indices are equal to 0 and all even are equal to 1
 */
__global__ void Assign_A_kernel(
	int M,
	int K,
	half *out_a) {

  int idx = blockDim.x * blockIdx.x + threadIdx.x;

  if (idx < M*K) {

    int row_major_row = idx / K;
    int row_major_col = idx % K;
    int col_major_index = row_major_col * M + row_major_row;

    if( idx % 2 == 0 ) {
      out_a[col_major_index] = 1.0;
    }
    else {
      out_a[col_major_index] = 0.0;
    }
  }

} 

/* Assign values of in_a to out_a
 *
*/
__global__ void Assign_A_values_kernel(
        int size,
        float *in_a,
        half *out_a) {

  int idx = blockDim.x * blockIdx.x + threadIdx.x;
  if (idx < size) {
    out_a[idx] = in_a[idx];
  }

}




int main(int argc, char* argv[]) {
   const int num_streams = MATRIX_M;
   cudaStream_t streams[num_streams];
   for(int stream_id = 0; stream_id < num_streams; stream_id++) {
     cudaStreamCreate(&streams[stream_id]);
   }

   float *a_fp32;
   float *b_fp32; // Store the b matrix in row major format "to emulate 1-hot lookups"
   float *b_fp32_col; // Store the b matrix in column major format for the other examples
   float *c;

   //Create the "batched" n-hot input vectors (in row major format for this case)
   float *a_fp32_host;
   a_fp32_host = (float*)malloc(MATRIX_M * MATRIX_K * sizeof(float));

   float *a_fp32_host_col;
   a_fp32_host_col = (float*)malloc(MATRIX_M * MATRIX_K * sizeof(float));

   //Keep track of the word ids
   int *pseudo_word_ids;
   pseudo_word_ids = (int*)malloc(MATRIX_M * sizeof(int));

   int *number_nonsparse_elem;
   number_nonsparse_elem = (int*)malloc(MATRIX_M * sizeof(int));
   memset(number_nonsparse_elem, 0, (MATRIX_M * sizeof(int)));
   memset(a_fp32_host, 0, (MATRIX_M * MATRIX_K * sizeof(float)));
   memset(a_fp32_host_col, 0, (MATRIX_M * MATRIX_K * sizeof(float)));

   int matrix_generated = 0;

   while( !matrix_generated ) {
     int break_flag = 1;

     //Section to randomnly generate a sparse input
     for(int iter_row = 0; iter_row < MATRIX_M; iter_row++) {
       //If batch element already contains enough sparse elements
       if( number_nonsparse_elem[iter_row] < NUM_SPARSE_ELEM) {

         break_flag = 0;

         for(int iter_col = 0; iter_col < MATRIX_K; iter_col++) {

           int index_row_major = iter_row * MATRIX_K + iter_col;
           int index_col_major = iter_col * MATRIX_M + iter_row;

           if( a_fp32_host[index_row_major] == 0) {
             int assign_val = rand() % 2;
             if( assign_val > 0 && number_nonsparse_elem[iter_row] < NUM_SPARSE_ELEM ) {
               pseudo_word_ids[iter_row] = iter_col;

               a_fp32_host[index_row_major] = 1;
               a_fp32_host_col[index_col_major] = 1;
               number_nonsparse_elem[iter_row]++;
             }
           }

         } // End processing columns
       }
     } // End processing rows

     if(break_flag) {
       matrix_generated = 1;
     }
   } // End while statement


   //Structure used to perform the sparse lookup
   GemmArguments gemm_input(MATRIX_M * MATRIX_K, MATRIX_K * MATRIX_N, MATRIX_M * MATRIX_N);


   //Create CSR representation for the first matrix (This is row major)

   int *a_row_host;
   int *a_col_host;
   float *a_values_host;

   a_row_host = (int*)malloc((MATRIX_M + 1) * sizeof(int));
   a_col_host = (int*)malloc(MATRIX_M * MATRIX_K * sizeof(int));
   a_values_host = (float*)malloc(MATRIX_M * MATRIX_K * sizeof(float));

   int csr_index = 0; // Also, the number of non zero elements (nnz)
   for(int iter_row = 0; iter_row < MATRIX_M; iter_row++) {
     a_row_host[iter_row] = csr_index;
     for(int iter_col = 0; iter_col < MATRIX_K; iter_col++) {
       int aux_index_row_major = iter_row * MATRIX_K + iter_col;
       if(a_fp32_host[aux_index_row_major] > 0) {

         a_col_host[csr_index] = iter_col;
         a_values_host[csr_index] = a_fp32_host[aux_index_row_major];
         csr_index++;

       }
     }
   }

   a_row_host[MATRIX_M] = csr_index;
 
   //Copy the CSR structure to the device 
   cudaErrCheck(cudaMemcpy(gemm_input.a_row.data().get(), a_row_host, (MATRIX_M + 1) * sizeof(int), cudaMemcpyHostToDevice));
   cudaErrCheck(cudaMemcpy(gemm_input.a_col.data().get(), a_col_host, csr_index * sizeof(int), cudaMemcpyHostToDevice));
   cudaErrCheck(cudaMemcpy(gemm_input.a_values.data().get(), a_values_host, csr_index * sizeof(float), cudaMemcpyHostToDevice));


 

   cudaDeviceSynchronize();
   #if DEBUG
     printf("Begin Execution \n");
   #endif

   //Variables used to store outputs
   float *c_wmma;
   float *c_one_hot;
   float *c_host_our_method;
   float *c_host_cublas;
   float *c_host_cusparse;
   float *c_host_one_hot_lookup;

   float cublasTime;
   float vanillaGEMMTime;
   float cusparseTime;
   float one_hot_time;

   //Variables used for timing
   curandGenerator_t gen;
   cublasHandle_t cublasHandle;

   cudaEvent_t startVanillaGEMM;
   cudaEvent_t stopVanillaGEMM;
    
   cudaEvent_t startWMMA;
   cudaEvent_t stopWMMA;
   
   cudaEvent_t startcublas;
   cudaEvent_t stopcublas;

   cudaEvent_t startOneHot;
   cudaEvent_t stopOneHot;

   cudaErrCheck(cudaEventCreate(&startVanillaGEMM));
   cudaErrCheck(cudaEventCreate(&stopVanillaGEMM));
   
   cudaErrCheck(cudaEventCreate(&startWMMA));
   cudaErrCheck(cudaEventCreate(&stopWMMA));
   
   cudaErrCheck(cudaEventCreate(&startcublas));
   cudaErrCheck(cudaEventCreate(&stopcublas));

   cudaErrCheck(cudaEventCreate(&startOneHot));
   cudaErrCheck(cudaEventCreate(&stopOneHot));
   
   
   cublasErrCheck(cublasCreate(&cublasHandle));
   
   // Default for no tensor core support and TENSOR_OP_MATH 
   // for tensor core support
   //cublasErrCheck(cublasSetMathMode(cublasHandle,CUBLAS_DEFAULT_MATH));
   cublasErrCheck(cublasSetMathMode(cublasHandle, CUBLAS_TENSOR_OP_MATH));
   
   cudaErrCheck(cudaMalloc((void**)&a_fp32, MATRIX_M * MATRIX_K * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)&b_fp32, MATRIX_K * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)&b_fp32_col, MATRIX_K * MATRIX_N * sizeof(float)));


   cudaErrCheck(cudaMalloc((void**)&c, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)&c_wmma, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMemset(c_wmma, 0, MATRIX_M * MATRIX_N * sizeof(float)));

   cudaErrCheck(cudaMalloc((void**)&c_one_hot, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMemset(c_one_hot, 0, MATRIX_M * MATRIX_N * sizeof(float)));


   //Allocate CPU variables
   c_host_our_method = (float*)malloc(MATRIX_M * MATRIX_N * sizeof(float));
   c_host_cublas = (float*)malloc(MATRIX_M * MATRIX_N * sizeof(float));
   c_host_cusparse = (float*)malloc(MATRIX_M * MATRIX_N * sizeof(float));
   c_host_one_hot_lookup = (float*)malloc( MATRIX_M * MATRIX_N * sizeof(float) );

   curandErrCheck(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
   curandErrCheck(curandSetPseudoRandomGeneratorSeed(gen, 1337ULL));

   
   //Copy the values of the column-major a matrix into the device
   cudaErrCheck(cudaMemcpy( a_fp32, a_fp32_host_col, MATRIX_M * MATRIX_K * sizeof(float), cudaMemcpyHostToDevice));

   curandErrCheck(curandGenerateUniform(gen, b_fp32, MATRIX_K * MATRIX_N));

   Assign_A_values_kernel <<< (csr_index + 255) / 256, 256>>> (
     csr_index,
     gemm_input.a_values.data().get(),
     gemm_input.a_values_half.data().get()
   );

   Assign_A_values_kernel <<< ((MATRIX_K * MATRIX_M) + 255) / 256, 256>>> (
     (MATRIX_K * MATRIX_M),
     a_fp32,
     gemm_input.a_fp16.data().get()
   );


   convertFp32ToFp16_column <<< (MATRIX_K * MATRIX_N + 255) / 256, 256 >>> (
     gemm_input.b_fp16.data().get(), 
     gemm_input.b_col_fp16.data().get(), 
     b_fp32_col,
     b_fp32, 
     (MATRIX_K * MATRIX_N), 
     MATRIX_K, 
     MATRIX_N
   );

   curandErrCheck(curandDestroyGenerator(gen));
   
   float alpha = 1.0f;
   float beta = 1.0f;

   #if DEBUG
     printf("\nM = %d, N = %d, K = %d. alpha = %f, beta = %f\n\n", MATRIX_M, MATRIX_N, MATRIX_K, alpha, beta);

   printf("Running with my sparse GEMM...\n");  
   #endif

   //First, use a matrix implementation
   cudaDeviceSynchronize();
   cudaErrCheck(cudaEventRecord(startVanillaGEMM));

   FirstLayerOperation(
     MATRIX_M,
     MATRIX_N,
     MATRIX_K,
     alpha,
     gemm_input.a_values_half.data().get(),
     gemm_input.a_row.data().get(),
     gemm_input.a_col.data().get(),
     MATRIX_K,
     gemm_input.b_fp16.data().get(),
     MATRIX_N,
     beta,
     c_wmma,
     MATRIX_N
   );

   cudaErrCheck(cudaEventRecord(stopVanillaGEMM));
   cudaErrCheck(cudaEventSynchronize(stopVanillaGEMM));
   cudaErrCheck(cudaEventElapsedTime(&vanillaGEMMTime, startVanillaGEMM, stopVanillaGEMM));

   cudaErrCheck(cudaMemcpy(c_host_our_method, c_wmma, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));

   cudaErrCheck(cudaMemset(c_wmma, 0, MATRIX_M * MATRIX_N * sizeof(float)));

   // Now using cuBLAS
   cudaDeviceSynchronize();
   #if DEBUG
     printf("Running with cuBLAS...\n");
   #endif

   cudaErrCheck(cudaEventRecord(startcublas));
   cublasErrCheck(cublasGemmEx(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, 
                MATRIX_M, MATRIX_N, MATRIX_K, 
                &alpha,
                gemm_input.a_fp16.data().get(), CUDA_R_16F, MATRIX_M,
                gemm_input.b_col_fp16.data().get(), CUDA_R_16F, MATRIX_K,
                &beta, 
                c_wmma, CUDA_R_32F, MATRIX_M,
                CUDA_R_32F, CUBLAS_GEMM_DFALT_TENSOR_OP));

   cudaErrCheck(cudaEventRecord(stopcublas));
   cudaErrCheck(cudaEventSynchronize(stopcublas));
   cudaErrCheck(cudaEventElapsedTime(&cublasTime, startcublas, stopcublas));
   


   // Error

   cudaErrCheck(cudaMemcpy(c_host_cublas, c_wmma, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));
   cudaErrCheck(cudaMemset(c_wmma, 0, MATRIX_M * MATRIX_N * sizeof(float)));


   //Now, run the matrix multiplication using the sparse format
   //using  cusparse<t>csrmm()
   #if DEBUG
     printf("Running with cuSPARSE\n");
   #endif

   cusparseHandle_t handle = 0;
   cusparseMatDescr_t descr = 0;

   cusparseCreate(&handle);
   cusparseCreateMatDescr(&descr);
   cusparseSetMatType(descr,CUSPARSE_MATRIX_TYPE_GENERAL);

   cudaErrCheck(cudaDeviceSynchronize());
   cudaErrCheck(cudaEventRecord(startWMMA));

   cusparseScsrmm(
     handle, 
     CUSPARSE_OPERATION_NON_TRANSPOSE, 
     MATRIX_M, 
     MATRIX_N, 
     MATRIX_K,
     csr_index,
     &alpha, 
     descr,
     gemm_input.a_values.data().get(), 
     gemm_input.a_row.data().get(), 
     gemm_input.a_col.data().get(),
     b_fp32_col,
     MATRIX_K,
     &beta, 
     c_wmma, 
     MATRIX_M
   );

   cudaErrCheck(cudaEventRecord(stopWMMA));
   cudaErrCheck(cudaEventSynchronize(stopWMMA));
   cudaErrCheck(cudaEventElapsedTime(&cusparseTime, startWMMA, stopWMMA));
   
   //End the matrix multiplication
   cudaErrCheck(cudaMemcpy(c_host_cusparse, c_wmma, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));

   // The sparse data structure in CSR format is row major while the rest are column-major
   #if NUM_SPARSE_ELEM == 1

   cudaErrCheck(cudaEventRecord(startOneHot));

   for(int batch_to_copy = 0; batch_to_copy < MATRIX_M; ++batch_to_copy) {
     cudaErrCheck(
       cudaMemcpyAsync(
         c_one_hot + (batch_to_copy * MATRIX_N), 
         b_fp32 + (pseudo_word_ids[batch_to_copy] * MATRIX_N), 
         MATRIX_N * sizeof(float), 
         cudaMemcpyDeviceToDevice)
     );
   }

   cudaErrCheck(cudaEventRecord(stopOneHot));
   cudaErrCheck(cudaEventSynchronize(stopOneHot));
   

   cudaErrCheck(cudaMemcpy(c_host_one_hot_lookup, c_one_hot, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));

   #endif


   // Error checking
   //Check the values of the three different implementations
   for(int iter_row = 0; iter_row < MATRIX_M; iter_row++) {
     for(int iter_col = 0; iter_col < MATRIX_N; iter_col++) {
       int access_index_col = iter_col * MATRIX_M + iter_row;
       int access_index_row =  iter_row * MATRIX_N + iter_col;


       float returned_val1 = c_host_our_method[access_index_row];
       float returned_val2 = c_host_cublas[access_index_col];
       float returned_val3 = c_host_cusparse[access_index_col];
       #if NUM_SPARSE_ELEM == 1
         float returned_val4 = c_host_one_hot_lookup[access_index_row];
       #endif
       
       float first_diff = fabs(returned_val1 - returned_val2);
       float second_dif = fabs(returned_val2 - returned_val3);
       float thrird_diff = fabs(returned_val1 - returned_val3);
       #if NUM_SPARSE_ELEM == 1
         float fourth_diff = fabs(returned_val2 - returned_val4);
       #endif

       float epsilon = 0.01;

       if(first_diff > epsilon || second_dif > epsilon || thrird_diff > epsilon) {
         #if DEBUG
           printf("The returned value for: our-cuBLAS-cuSPARSE value is %f / %f / %f and the index is [%d,%d]\n",returned_val1, returned_val2, returned_val3, iter_row, iter_col);
         #endif
       }

       #if NUM_SPARSE_ELEM == 1
         if(fourth_diff > epsilon) {
           printf("The returned value for: one_hot-cuBLAS is %f / %f and the index is [%d,%d]\n",returned_val4, returned_val2, iter_row, iter_col);
         }
       #endif

     }
   }




   #if DEBUG
     printf("Done with execution.\n\n");
   #endif


   //Times are in ms
   printf("%f\n", vanillaGEMMTime); //Our method
   printf("%f\n", cublasTime); // The cuBLAS time
   printf("%f\n", cusparseTime); // The cuSPARSE time

   #if NUM_SPARSE_ELEM == 1
     cudaErrCheck(cudaEventElapsedTime(&one_hot_time, startOneHot, stopOneHot));
     printf("one-hot lookup took %fms\n", one_hot_time);
   #endif
  
   cudaDeviceSynchronize(); 
   cudaErrCheck(cudaEventDestroy(startWMMA));
   cudaErrCheck(cudaEventDestroy(stopWMMA));

   cudaErrCheck(cudaEventDestroy(startcublas));             
   cudaErrCheck(cudaEventDestroy(stopcublas));

   cudaErrCheck(cudaEventDestroy(startOneHot));
   cudaErrCheck(cudaEventDestroy(stopOneHot));


   cudaDeviceSynchronize();
   cudaErrCheck(cudaFree(a_fp32));
   cudaErrCheck(cudaFree(b_fp32));
   cudaErrCheck(cudaFree(b_fp32_col));

   gemm_input.~GemmArguments();
   cudaDeviceSynchronize();

   cudaErrCheck(cudaFree(c));
   cudaErrCheck(cudaFree(c_wmma));
   cudaErrCheck(cudaFree(c_one_hot)); 
  
   cudaDeviceSynchronize();

   free(a_fp32_host);
   free(a_fp32_host_col);
   free(c_host_our_method); 
   free(c_host_cublas);
   free(c_host_cusparse);
   free(c_host_one_hot_lookup);

   cudaDeviceSynchronize();

   for(int stream_id = 0; stream_id < num_streams; stream_id++) {
     cudaStreamDestroy(streams[stream_id]);
   }

   cudaErrCheck(cudaDeviceReset());
   return 0;
}



