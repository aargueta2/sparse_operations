/* Copyright (c) 1993-2017, NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <curand.h>
#include <cublas_v2.h>

#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include "cuda_profiler_api.h"
#include "../include/nvidia_functions.h"

__device__ float atomicMax(float * const address, const float value)
{
	if (* address >= value)
	{
		return *address;
	}

	int * const address_as_i = (int *)address;
	int old = * address_as_i, assumed;

	do 
	{
		assumed = old;
		if (__int_as_float(assumed) >= value)
		{
			break;
		}

		old = atomicCAS(address_as_i, assumed, __float_as_int(value));
	} while (assumed != old);

        return __int_as_float(old);
}


// Define some error checking macros.
#define cudaErrCheck(stat) { cudaErrCheck_((stat), __FILE__, __LINE__); }
void cudaErrCheck_(cudaError_t stat, const char *file, int line) {
   if (stat != cudaSuccess) {
      fprintf(stderr, "CUDA Error: %s %s %d\n", cudaGetErrorString(stat), file, line);
   }
}

#define cublasErrCheck(stat) { cublasErrCheck_((stat), __FILE__, __LINE__); }
void cublasErrCheck_(cublasStatus_t stat, const char *file, int line) {
   if (stat != CUBLAS_STATUS_SUCCESS) {
      fprintf(stderr, "cuBLAS Error: %d %s %d\n", stat, file, line);
   }
}

#define curandErrCheck(stat) { curandErrCheck_((stat), __FILE__, __LINE__); }
void curandErrCheck_(curandStatus_t stat, const char *file, int line) {
   if (stat != CURAND_STATUS_SUCCESS) {
      fprintf(stderr, "cuRand Error: %d %s %d\n", stat, file, line);
   }
}


//using namespace nvcuda;
using namespace thrust::placeholders;

// Must be multiples of 16 for wmma code to work
#define MATRIX_M 10240
#define MATRIX_N 1024 //4096 //Batch size
#define MATRIX_K 512//4096

#define  TOP_K 400
#define  DIM_PRINT 1
//Convert the single precission input to half
__global__ void convertFp32ToFp16 (half *out, float *in, int n) {
   int idx = blockDim.x * blockIdx.x + threadIdx.x;
   if (idx < n) {
      out[idx] = in[idx];
   }
}



//typedef uint64_t prob_ptr_t;
typedef unsigned long long int prob_ptr_t;
typedef float prob_ptr_float_t;


__host__ __device__ prob_ptr_t pack (float prob, int ptr) {
  //assert (!isnan(prob));
  //assert (ptr >= 0 && ptr < 1L<<32);
  uint32_t i_prob = *(uint32_t *)&prob;
  if (i_prob & 0x80000000)
    i_prob = i_prob ^ 0xFFFFFFFF;
  else
    i_prob = i_prob ^ 0x80000000;
  return (uint64_t)i_prob << 32 | ptr;
}


// Unpacks a probability.
__host__ __device__ float unpack_prob (prob_ptr_t packed) {
  uint32_t i_prob = packed >> 32;
  if (i_prob & 0x80000000)
    i_prob = i_prob ^ 0x80000000;
  else
    i_prob = i_prob ^ 0xFFFFFFFF;
  return *(float *)&i_prob;
}

// Unpacks a back-pointer.
__host__ __device__ int unpack_ptr (prob_ptr_t packed) {
  //assert (!(packed & 0x80000000));
  return packed & 0x7FFFFFFF;
}


/// This class stores the input to the affine transform
class GemmArguments {

  public:
    thrust::device_vector<half> a_fp16;
    thrust::device_vector<half> b_fp16;
    thrust::device_vector<float> c_cublas;
    thrust::device_vector<int> index_data;

    // Bucketsort parameters
    int max_val;
    int min_val;     

    GemmArguments(int a_dim, int b_dim, int c_dim) {
      a_fp16.resize(a_dim, 0);
      b_fp16.resize(b_dim, 0);
      c_cublas.resize(c_dim, 0);
      index_data.resize((TOP_K * MATRIX_N), 0);

      max_val = 0;
      min_val = 0;
    }

    ~GemmArguments() {
      a_fp16.clear();
      a_fp16.shrink_to_fit();

      b_fp16.clear();
      b_fp16.shrink_to_fit();

      c_cublas.clear();
      c_cublas.shrink_to_fit();

      index_data.clear();
      index_data.shrink_to_fit();
    }
};

/// This method is used to normalize the values in an array/pointer
/// This method implements the naive softmax seen here:
/// https://arxiv.org/pdf/1805.02867.pdf
__global__ void Normalize_kernel(
  float *max_elem_device,
  float *sum_elements,
  int num_elements) {

  int idx = threadIdx.x + blockIdx.x * blockDim.x;

  if(idx < num_elements) {
    int batch_index = idx / TOP_K;
    //printf("The sum for %d is %f \n",batch_index,sum_elements[batch_index]);
    max_elem_device[idx] = max_elem_device[idx] / sum_elements[batch_index];
  }

}

/// Naive reference GEMM computation.
/// This implementation computes an affine transformation
/// and returns the largest element in the entire result
__global__ void ReferenceGemm_kernel(
  int M,
  int N,
  int K,
  float alpha,
  half const *A,
  int lda,
  half const *B,
  int ldb,
  float beta,
  float *C,
  int ldc,
  float *max_elem_device,
  int *index_data) {

  __shared__ float shared_top[MATRIX_N];
 
  if(threadIdx.x < MATRIX_N) {
    shared_top[threadIdx.x] = -1.0;
  }
  __syncthreads();

  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;

  if (i < M && j < N) {
    float accumulator = 0;

    #pragma unroll
    for (int k = 0; k < K; ++k) {
      accumulator += float(A[i + k * lda]) * float(B[k + j * ldb]);
    }

    accumulator = (alpha * accumulator + beta * C[i + j * ldc]);

    //Commenting out this part increases the execution time (It is not needed for the top-1 part)
    //C[i + j * ldc] = alpha * accumulator + beta * C[i + j * ldc];

    atomicMax(&shared_top[j], (alpha * accumulator + beta * C[i + j * ldc]));
  }

  __syncthreads();
  if(threadIdx.x == 0) {
    #pragma unroll
    for(int thread_iter = 0; thread_iter < MATRIX_N; thread_iter++) {
      atomicMax(&max_elem_device[thread_iter],shared_top[thread_iter]);
    }
  }
}

//#define THREADBLOCK_SIZE 256
//#define THREAD_ELEM (MATRIX_M-1)/(THREADBLOCK_SIZE+1)

__device__ int ceildiv(size_t x, size_t y) { return (x-1)/y+1; }

__device__ __forceinline__ float max_op(float a, float b)
{
    return fmaxf(a, b);
}









/// Naive reference GEMM computation.
template<int MAX_K, int THREADBLOCK_SIZE>
__launch_bounds__(THREADBLOCK_SIZE)
__global__ void ReferenceGemm_kernel_topK(
  int M,
  int N,
  int K,
  float alpha,
  const half  *__restrict A,
  int lda,
  const half * __restrict B,
  int ldb,
  float beta,
  const float * __restrict C,
  int ldc,
  float * z,
  int * z_index) {


    int thread_id = threadIdx.x;
    int vector_id = blockIdx.x;

    //Declare CUB variables  
    typedef cub::BlockReduce<float, THREADBLOCK_SIZE> MaxValBlockReduce;
    typedef cub::BlockReduce<TopKD_Compressed<MAX_K>, THREADBLOCK_SIZE> BlockReduce;

    __shared__ typename MaxValBlockReduce::TempStorage max_val_temp_storage;
    __shared__ typename BlockReduce::TempStorage temp_storage;

    //Variables used to store the top-k elements per thread
    TopKD_Compressed<MAX_K> partial;
    for(int i = 0; i < MAX_K; ++i)
        partial.topk.p[i] = pack(-FLT_MAX, -1);

    partial.d = 0.0F;

    __syncthreads();

    float accumulator = 0;

    //Execute matrix multiplication and top-k selection
    #pragma unroll
    for(int a_col = thread_id; a_col < M; a_col += THREADBLOCK_SIZE) {

        accumulator = 0;

        #pragma unroll
        for (int k = 0; k < K; ++k) {
          accumulator += float(A[a_col + k * lda]) * float(B[k + vector_id * ldb]);
        }

        float c_col_val = alpha * accumulator + beta * C[a_col + vector_id * ldc];
        partial.d += c_col_val;
        partial.topk.insert(pack(c_col_val, a_col)); 

    }  
    
    //Obtain the top-k elements at the block level
    TopKD_Compressed<TOP_K> total = BlockReduce(temp_storage).Reduce(partial, reduce_topk_d_op_compressed<TOP_K>);
    //Use thread 0 to write the top-k elements
    if (thread_id == 0)
    {
      z += vector_id * TOP_K;
      for(int i = 0; i < TOP_K; ++i) {
        unsigned long long int ret_val_top_k = total.topk.p[i];
        float prob_ret = unpack_prob(ret_val_top_k);
        int index_ret = unpack_ptr(ret_val_top_k);
        z[i] = float(prob_ret);
        z_index[i] = index_ret;
      }
    }

}


/// Naive reference GEMM computation.
template<int MAX_K, int THREADBLOCK_SIZE>
__launch_bounds__(THREADBLOCK_SIZE)
__global__ void ReferenceGemm_kernel_topK_over_50(
  int M,
  int N,
  int K,
  float alpha,
  const half  *__restrict A,
  int lda,
  const half * __restrict B,
  int ldb,
  float beta,
  const float * __restrict C,
  int ldc,
  float * z,
  int * z_index) {


    int thread_id = threadIdx.x;
    int vector_id = blockIdx.x;
    __shared__ prob_ptr_t top_k_shared[TOP_K];
    __shared__ float total_sum_val;

    #pragma unroll
    for(int fill_top = thread_id; fill_top < TOP_K; fill_top+= blockDim.x) {
      top_k_shared[fill_top] = pack(float(-FLT_MAX),0);
    }

    total_sum_val = 0;

    __syncthreads();

  prob_ptr_t total_value_entry_row_i = 0;
  float accumulator = 0;


    #pragma unroll
    for(int a_col = thread_id; a_col < M; a_col += THREADBLOCK_SIZE) {

        accumulator = 0;

        #pragma unroll
        for (int k = 0; k < K; ++k) {
          accumulator += float(A[a_col + k * lda]) * float(B[k + vector_id * ldb]);
        }

        float c_col_val = alpha * accumulator + beta * C[a_col + vector_id * ldc];
        total_value_entry_row_i = pack( c_col_val , a_col);
     
        atomicAdd(&total_sum_val, c_col_val);

        //Simpler version for the swap
        for(int iter_col = 0; iter_col < TOP_K; iter_col++){
          prob_ptr_t ret_temp_val = atomicMax(&top_k_shared[iter_col], total_value_entry_row_i);
          if(ret_temp_val < total_value_entry_row_i) {
            total_value_entry_row_i = ret_temp_val;
          }
        }

      
    }  

    __syncthreads();


    if (thread_id < TOP_K)
    {
      z += vector_id * TOP_K;
      float prob_ret = unpack_prob(top_k_shared[thread_id]);
      int index_ret = unpack_ptr(top_k_shared[thread_id]);
      z[thread_id] = float(prob_ret); //  / float(total_sum_val);
      z_index[thread_id] = index_ret;
    }

}


/// Reference GEMM computation.
float ReferenceGemm(
  int M,
  int N,
  int K,
  float alpha,
  const half *A,
  int lda,
  const half *B,
  int ldb,
  float beta,
  const float *C,
  int ldc,
  float* max_elem_device,
  float* max_elem_host,
  int* index_data) {
  
  //Block dimensions for the kernel call
  dim3 block(256);
  dim3 grid(
    MATRIX_N
  );

  cudaEvent_t startTopK;
  cudaEvent_t stopTopK;

  //Create the timing events
  cudaErrCheck(cudaEventCreate(&startTopK));
  cudaErrCheck(cudaEventCreate(&stopTopK));

  //Record the top-k methods
  cudaErrCheck(cudaEventRecord(startTopK));

  #if TOP_K == 1
    // Getting the max element per batch is simpler
    ReferenceGemm_kernel<<< grid, block >>>(M, N, K, alpha, A, lda, B, ldb, beta, C, ldc, max_elem_device,sum_elements);
  #else
    // Obtaining the top-k elements is more complicated
    ReferenceGemm_kernel_topK_over_50<TOP_K,256><<< MATRIX_N, 256>>>(M, N, K, alpha, A, lda, B, ldb, beta, C, ldc,max_elem_device, index_data); 
  #endif

  cudaErrCheck(cudaEventRecord(stopTopK));
  cudaErrCheck(cudaEventSynchronize(stopTopK));
  cudaDeviceSynchronize();

  //Record the tota execution time
  float elapsedTopKTime;
  cudaErrCheck(cudaEventElapsedTime(&elapsedTopKTime, startTopK, stopTopK));

  //Obtain the time to copy the elements back to the host CPU
  cudaEvent_t startTopK_aux;
  cudaEvent_t stopTopK_aux;

  cudaErrCheck(cudaEventCreate(&startTopK_aux));
  cudaErrCheck(cudaEventCreate(&stopTopK_aux));

  cudaErrCheck(cudaEventRecord(startTopK_aux));

  //Copy the required elements back to the host
  cudaMemcpy(max_elem_host, max_elem_device, TOP_K * MATRIX_N * sizeof(float),cudaMemcpyDeviceToHost);

  cudaErrCheck(cudaEventRecord(stopTopK_aux));
  cudaErrCheck(cudaEventSynchronize(stopTopK_aux));

  return elapsedTopKTime;
}



__global__ void softmax_topk(
    const float * __restrict x,
    float * __restrict v,
    int * __restrict z,
    int V,
    int K)
{
    //Declare variables in shared memory
    __shared__ prob_ptr_t top_k_shared[TOP_K];
    __shared__ float total_sum_val;
    __shared__ float max_block_val;

    int thread_id;
    int vector_id;
    float read_elem;

    thread_id = threadIdx.x;
    vector_id = blockIdx.x;

    //Pointer arithmetic to index elements in a batch easier
    x += vector_id * V;

    //Initialize the aux memory
    #pragma unroll
    for(int fill_top = thread_id; fill_top < TOP_K; fill_top+= blockDim.x) {
      top_k_shared[fill_top] = pack(float(-FLT_MAX),0);
    }

    max_block_val = -FLT_MAX;
    total_sum_val = 0.0F;

    __syncthreads();

    prob_ptr_t total_value_entry_row_i = 0;

    //Iterate over all the batch elements
    for(int elem_id = thread_id; elem_id < V; elem_id += blockDim.x)
    {
      read_elem = x[elem_id];
      total_value_entry_row_i = pack( read_elem , elem_id);

      atomicMax(&max_block_val, read_elem);

      int index_access;
      //Simpler version for the swap
      for(int iter_col = 0; iter_col < TOP_K; iter_col++){
        //Use a "hashing" to have a different access pattern
        index_access = (thread_id + iter_col) % TOP_K; // TODO: Use this for random access (avoid collision)
        //index_access = iter_col; // Use this variable for common access (causes collision)

        prob_ptr_t ret_temp_val = atomicMax(&top_k_shared[index_access], total_value_entry_row_i);
        if(ret_temp_val < total_value_entry_row_i) {
          total_value_entry_row_i = ret_temp_val;
        }
      }
    }

    __syncthreads();

    //Calculate the softmax denominator
    for(int elem_id = thread_id; elem_id < V; elem_id += blockDim.x)
    {
      read_elem = x[elem_id];
      atomicAdd(&total_sum_val, __expf(float(read_elem - max_block_val)));
    }

    __syncthreads();

    // Write the softmax elements back to their corresponding variables
    if (thread_id < TOP_K)
    {
      z += vector_id * TOP_K;
      v += vector_id * TOP_K;
      float prob_ret = unpack_prob(top_k_shared[thread_id]);
      int index_ret = unpack_ptr(top_k_shared[thread_id]);
      float d_total_inverse = __fdividef(1.0F, float(total_sum_val));

      v[thread_id] = __expf(float(prob_ret - max_block_val)) * d_total_inverse;
      z[thread_id] = index_ret;
    }

}


//Our top-k method
float softmax_topK_mine(
  float *in_values,
  float *out_probabilities,
  int *index,
  int num_elements,
  int top_k,
  int batch_size,
  float *x_host) {

  cudaEvent_t startTopK;
  cudaEvent_t stopTopK;
  CUDA_CHECK(cudaEventCreate(&startTopK));
  CUDA_CHECK(cudaEventCreate(&stopTopK));

  CUDA_CHECK(cudaEventRecord(startTopK));

  softmax_topk<<<batch_size,256>>>(in_values, out_probabilities, index, num_elements, top_k);

  CUDA_CHECK(cudaEventRecord(stopTopK));
  CUDA_CHECK(cudaEventSynchronize(stopTopK));
  float TopKTime;
  CUDA_CHECK(cudaEventElapsedTime(&TopKTime, startTopK, stopTopK));

  CUDA_CHECK(cudaMemcpy(x_host, out_probabilities,(size_t)top_k * batch_size * sizeof(float),cudaMemcpyDeviceToHost));
  return TopKTime;
}



int main(int argc, char* argv[]) {
   float *a_fp32;
   float *b_fp32;
   float *c;

   GemmArguments gemm_input(MATRIX_M * MATRIX_K, MATRIX_K * MATRIX_N, MATRIX_M * MATRIX_N);

   cudaDeviceSynchronize();

   float **c_wmma;
   float **c_wmma_placeholder;
   float *max_element;
   float **max_element_gemm;
   int **max_element_gemm_index;

   c_wmma = (float**)malloc( sizeof(float));
   c_wmma_placeholder = (float**)malloc( sizeof(float));
   max_element_gemm = (float**)malloc( sizeof(float));
   max_element_gemm_index = (int**)malloc( sizeof(int));

   float *c_host_cublas;
   float *c_host_wmma;
   float *max_elem_host;
   float *max_elem_host2;
   float *max_elem_host_baseline;
  
 
   curandGenerator_t gen;
   cublasHandle_t cublasHandle;
    
   cudaEvent_t startcublas;
   cudaEvent_t stopcublas;

   cudaErrCheck(cudaEventCreate(&startcublas));
   cudaErrCheck(cudaEventCreate(&stopcublas));
   cublasErrCheck(cublasCreate(&cublasHandle));
   
   // Default for no tensor core support and TENSOR_OP_MATH 
   // for tensor core support
   //cublasErrCheck(cublasSetMathMode(cublasHandle,CUBLAS_DEFAULT_MATH));
   cublasErrCheck(cublasSetMathMode(cublasHandle, CUBLAS_TENSOR_OP_MATH));
   
   cudaErrCheck(cudaMalloc((void**)&a_fp32, MATRIX_M * MATRIX_K * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)&b_fp32, MATRIX_K * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)&c, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)c_wmma, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)c_wmma_placeholder, MATRIX_M * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMemset(*c_wmma_placeholder, 0, MATRIX_M * MATRIX_N * sizeof(float)));

   cudaErrCheck(cudaMalloc((void**)&max_element, MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMemset(max_element, 0, MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)max_element_gemm, TOP_K * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMemset(*max_element_gemm, 0, TOP_K * MATRIX_N * sizeof(float)));
   cudaErrCheck(cudaMalloc((void**)max_element_gemm_index, TOP_K * MATRIX_N * sizeof(int)));
   cudaErrCheck(cudaMemset(*max_element_gemm_index, 0, TOP_K * MATRIX_N * sizeof(int)));

   //Allocate CPU variables
   c_host_cublas = (float*)malloc(MATRIX_M * MATRIX_N * sizeof(float));
   c_host_wmma = (float*)malloc(MATRIX_M * MATRIX_N * sizeof(float));
   max_elem_host = (float*)malloc( TOP_K * MATRIX_N * sizeof(float));
   max_elem_host2 = (float*)malloc( TOP_K * MATRIX_N * sizeof(float));
   max_elem_host_baseline = (float*)malloc( TOP_K * MATRIX_N * sizeof(float));

   curandErrCheck(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
   curandErrCheck(curandSetPseudoRandomGeneratorSeed(gen, 1337ULL));

   curandErrCheck(curandGenerateUniform(gen, a_fp32, MATRIX_M * MATRIX_K));
   curandErrCheck(curandGenerateUniform(gen, b_fp32, MATRIX_K * MATRIX_N));

   // curand doesn't currently support fp16 so we generate in fp32 and convert to fp16.
   convertFp32ToFp16 <<< (MATRIX_M * MATRIX_K + 255) / 256, 256 >>> (gemm_input.a_fp16.data().get(), a_fp32, MATRIX_M * MATRIX_K);
   convertFp32ToFp16 <<< (MATRIX_K * MATRIX_N + 255) / 256, 256 >>> (gemm_input.b_fp16.data().get(), b_fp32, MATRIX_K * MATRIX_N);

   curandErrCheck(curandGenerateUniform(gen, c, MATRIX_M * MATRIX_N));
   curandErrCheck(curandDestroyGenerator(gen));
   
   cudaErrCheck(cudaMemcpy(gemm_input.c_cublas.data().get(), c, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToDevice));
   cudaErrCheck(cudaMemcpy(*c_wmma, c, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToDevice));

   //Begin calling CUDA code
   cudaDeviceSynchronize();

   float alpha = 2.0f;
   float beta = 2.0f;
   float fused_time_method = 0;
 
   //Print dimension info for debugging
   #if DIM_PRINT
     printf("\nM = %d, N = %d, K = %d. alpha = %f, beta = %f\n\n", MATRIX_M, MATRIX_N, MATRIX_K, alpha, beta);
   #endif

   cudaDeviceSynchronize(); 
   //First, use my matrix-softmax-top-k implementation

   fused_time_method = ReferenceGemm(MATRIX_M, 
		 MATRIX_N, 
		 MATRIX_K, 
		 alpha, 
		 gemm_input.a_fp16.data().get(), 
		 MATRIX_M, 
		 gemm_input.b_fp16.data().get(), 
		 MATRIX_K, 
		 beta, 
		 *c_wmma, 
		 MATRIX_M,
		 *max_element_gemm,
 		 max_elem_host,
                 gemm_input.index_data.data().get() );


   cudaDeviceSynchronize();

   //Time basic matrix multiplication using CuBlas' code
   cudaErrCheck(cudaEventRecord(startcublas));

   cublasErrCheck(cublasGemmEx(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N,
                MATRIX_M, MATRIX_N, MATRIX_K,
                &alpha,
                gemm_input.a_fp16.data().get(), CUDA_R_16F, MATRIX_M,
                gemm_input.b_fp16.data().get(), CUDA_R_16F, MATRIX_K,
                &beta,
                *c_wmma, CUDA_R_32F, MATRIX_M,
                CUDA_R_32F, CUBLAS_GEMM_DFALT_TENSOR_OP));

   cudaErrCheck(cudaEventRecord(stopcublas));
   cudaErrCheck(cudaEventSynchronize(stopcublas));
   //Time to execute the cublas multiplication time
   float cublasTime;
   cudaErrCheck(cudaEventElapsedTime(&cublasTime, startcublas, stopcublas));

   float softmax_top_time = 0.0;
   cudaDeviceSynchronize();

   //This method measures our top-k softmax method
   softmax_top_time = softmax_topK_mine(*c_wmma, *max_element_gemm, *max_element_gemm_index, MATRIX_M, TOP_K, MATRIX_N, max_elem_host2);

   cudaDeviceSynchronize();

   //Run the NVIDIA top-k selection
   float top_k_nvidia_time = 0.0;
   #if TOP_K >= 1
     //Measure NVIDIA's time to obtain the top-k and the softmax
     top_k_nvidia_time = nvidia_softmax_topK_online(c_wmma, max_element_gemm_index, max_element_gemm, MATRIX_M, TOP_K, MATRIX_N, max_elem_host_baseline);

   #else

     for(int cublas_max_iter = 0; cublas_max_iter < MATRIX_N; cublas_max_iter++) {

       thrust::device_vector<float>::iterator iter = thrust::max_element(
         gemm_input.c_cublas.begin() + (cublas_max_iter * MATRIX_M), 
         gemm_input.c_cublas.begin() + (cublas_max_iter * MATRIX_M + MATRIX_M)
       );

       float max_val = *iter;
       std::cout << "The maximum value is " << max_val << std::endl;
     }

   #endif

   cudaDeviceSynchronize();

   // Error checking
   cudaErrCheck(cudaMemcpy(c_host_wmma, *c_wmma, MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));
   cudaErrCheck(cudaMemcpy(c_host_cublas, gemm_input.c_cublas.data().get(), MATRIX_M * MATRIX_N * sizeof(float), cudaMemcpyDeviceToHost));
 
   // Total time to do the affine transform and top-k softmax with NVIDIA's methof
   float nvidia_total_time =  (top_k_nvidia_time + cublasTime);
   //My method with the time it takes to execute the affine transfom
   float my_unfused_time = (cublasTime + softmax_top_time);

   //Our method (No multiplication)
   printf("%f\n",softmax_top_time);
   //Baseline method (No multiplication)
   printf("%f\n", top_k_nvidia_time);


   //Do the check here (My implementation vs the NVIDIA baseline).
   for(int batch_elem = 0; batch_elem < MATRIX_N; batch_elem++) {
     for(int beam_elem = 0; beam_elem < TOP_K; beam_elem++) {
       int print_index = batch_elem * TOP_K + beam_elem;
       float max_value = std::max(max_elem_host[print_index], max_elem_host_baseline[print_index]);
       float min_value = std::min(max_elem_host[print_index], max_elem_host_baseline[print_index]);
       float abs_diff = std::abs(max_value - min_value);

       float max_value2 = std::max(max_elem_host2[print_index], max_elem_host_baseline[print_index]);
       float min_value2 = std::min(max_elem_host2[print_index], max_elem_host_baseline[print_index]);
       float abs_diff2 = std::abs(max_value2 - min_value2);
  
       #if DIM_PRINT
         if(abs_diff > float(0.1)) {
           printf("Print batch element %d \n",batch_elem);
           printf("The max for col %d is %f - %f\n",batch_elem, max_elem_host_baseline[print_index],max_elem_host[print_index]);
         }
         if( abs_diff2 > float(0.1)) {
           printf("Print batch element %d \n",batch_elem);
           printf("The max for col %d is %f / %f - %f\n",batch_elem, max_elem_host[print_index], max_elem_host_baseline[print_index], max_elem_host2[print_index]);
         }
       #endif
     }
   }



  
   cudaDeviceSynchronize(); 
   cudaErrCheck(cudaEventDestroy(startcublas));             
   cudaErrCheck(cudaEventDestroy(stopcublas));

   cudaDeviceSynchronize();
   cudaErrCheck(cudaFree(a_fp32));
   cudaErrCheck(cudaFree(b_fp32));


   gemm_input.~GemmArguments();

   cudaDeviceSynchronize();

   cudaErrCheck(cudaFree(c));
   cudaErrCheck(cudaFree(*c_wmma));
   cudaErrCheck(cudaFree(*c_wmma_placeholder));
  
   cudaDeviceSynchronize();
 
   free(c_host_cublas);
   free(max_elem_host);
   free(max_elem_host2);
   free(max_elem_host_baseline);

   cudaDeviceSynchronize();

   free(c_host_wmma);

   cudaDeviceSynchronize();
   cudaErrCheck(cudaDeviceReset());

   return 0;
}

