#include "../include/nvidia_functions.h"

// Method used to fill a vector with random values
void fill_random_values(float * x, int count)
{
    curandGenerator_t gen;
    CURAND_CHECK(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));
    CURAND_CHECK(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));
    CURAND_CHECK(curandGenerateUniform(gen, x, count));
    CURAND_CHECK(curandDestroyGenerator(gen));
}

// Max operator
__device__ __forceinline__ float max_op(float a, float b)
{
    return fmaxf(a, b);
}

//Method to compute the safe softmax as seen in algorithm 2 of the nvidia paper
template<int MAX_K, int THREADBLOCK_SIZE>
__launch_bounds__(THREADBLOCK_SIZE)
__global__ void safe_topk(
    const float * __restrict x,
    int * __restrict z,
    float * __restrict v,
    int V,
    int K)
{
    int thread_id = threadIdx.x;
    int vector_id = blockIdx.x;

    // reposition y to data for the current vector
    x += vector_id * V;

    typedef cub::BlockReduce<float, THREADBLOCK_SIZE> MaxValBlockReduce;
    typedef cub::BlockReduce<TopKD<MAX_K>, THREADBLOCK_SIZE> BlockReduce;

    __shared__ typename BlockReduce::TempStorage temp_storage;
    __syncthreads();

    TopKD<MAX_K> partial;
    for(int i = 0; i < MAX_K; ++i)
        partial.topk.p[i] = -1;
    for(int i = 0; i < MAX_K; ++i)
        partial.topk.u[i] = -FLT_MAX;
    partial.d = 0.0F;

    //Sort the top-k elements using thread-local memory
    for(int elem_id = thread_id; elem_id < V; elem_id += THREADBLOCK_SIZE)
    {
        float elem = x[elem_id];
        partial.d += elem;
        partial.topk.insert(elem, elem_id);
    }

    //Sort top-k elements using block memory
    TopKD<MAX_K> total = BlockReduce(temp_storage).Reduce(partial, reduce_topk_d_op<MAX_K>);

    //Write the top-k elements to device memory
    if (thread_id == 0)
    {
        z += vector_id * K;
        v += vector_id * K;

        float d_total_inverse = __fdividef(1.0F, total.d);
        for(int i = 0; i < MAX_K; ++i)
        {
            float val = total.topk.u[i];
            if (i < K)
            {
                z[i] = total.topk.p[i];
                v[i] = val;
            }
        }
    }
}

struct __align__(8) MD
{
    float m;
    float d;
};

template<int MAX_K>
struct TopKMD
{
    MD md;
    TopK<MAX_K> topk;
};

__device__ __forceinline__ MD reduce_md_op(MD a, MD b)
{
    bool a_bigger = (a.m > b.m);
    MD bigger_m = a_bigger ? a : b;
    MD smaller_m = a_bigger ? b : a;
    MD res;
    res.d = bigger_m.d + smaller_m.d * __expf(smaller_m.m - bigger_m.m);
    res.m = bigger_m.m;
    return res;
}

template<int MAX_K>
__device__ __forceinline__ TopKMD<MAX_K> reduce_topk_md_op(const TopKMD<MAX_K>& a, const TopKMD<MAX_K>& b)
{
    TopKMD<MAX_K> res;
    res.md = reduce_md_op(a.md, b.md);
    res.topk = reduce_topk_op(a.topk, b.topk);
    return res;
}

//This method computes algorithm 3 on the softmax paper
template<int MAX_K, int THREADBLOCK_SIZE>
__launch_bounds__(THREADBLOCK_SIZE)
__global__ void online_softmax_topk(
    const float * __restrict x,
    int * __restrict z,
    float * __restrict v,
    int V,
    int K)
{
    int thread_id = threadIdx.x;
    int vector_id = blockIdx.x;

    // reposition y to data for the current vector
    x += vector_id * V;

    typedef cub::BlockReduce<TopKMD<MAX_K>, THREADBLOCK_SIZE> BlockReduce;

    __shared__ typename BlockReduce::TempStorage temp_storage;

    TopKMD<MAX_K> partial;
    for(int i = 0; i < MAX_K; ++i)
        partial.topk.p[i] = -1;
    for(int i = 0; i < MAX_K; ++i)
        partial.topk.u[i] = -FLT_MAX;
    partial.md.m = -FLT_MAX;
    partial.md.d = 0.0F;
    for(int elem_id = thread_id; elem_id < V; elem_id += THREADBLOCK_SIZE)
    {
        float elem = x[elem_id];
        MD new_elem{elem, 1.0F};
        partial.md = reduce_md_op(partial.md, new_elem);
        partial.topk.insert(elem, elem_id);
    }

    TopKMD<MAX_K> total = BlockReduce(temp_storage).Reduce(partial, reduce_topk_md_op<MAX_K>);

    if (thread_id == 0)
    {
        z += vector_id * K;
        v += vector_id * K;
        
        float d_total_inverse = __fdividef(1.0F, total.md.d);
        for(int i = 0; i < MAX_K; ++i)
        {
            float val = __expf(total.topk.u[i] - total.md.m) * d_total_inverse;
            if (i < K)
            {
                z[i] = total.topk.p[i];
                v[i] = val;
            }
        }
    }
}

//This method is used to time the safe softmax and top-k. It also returns the execution time
//As seen on algorithm 2 on the nvidia paper
float nvidia_softmax_topK(
  float **values,
  int **index,
  float **probabilities,
  int num_elements,
  int top_k,
  int batch_size,
  float * x_host) {

  cudaEvent_t startTopK;
  cudaEvent_t stopTopK;
  CUDA_CHECK(cudaEventCreate(&startTopK));
  CUDA_CHECK(cudaEventCreate(&stopTopK));

  CUDA_CHECK(cudaEventRecord(startTopK));
  safe_topk<MAX_K,256><<<batch_size,256>>>(*values, *index, *probabilities, num_elements, top_k);

  CUDA_CHECK(cudaEventRecord(stopTopK));
  CUDA_CHECK(cudaEventSynchronize(stopTopK));
  float TopKTime;
  CUDA_CHECK(cudaEventElapsedTime(&TopKTime, startTopK, stopTopK));
  cudaDeviceSynchronize();

  CUDA_CHECK(cudaMemcpy(x_host,*probabilities,(size_t)top_k * batch_size * sizeof(float),cudaMemcpyDeviceToHost));
  return TopKTime;
}


//This method calls the online top-k method
//The routine returns the execution time for the softmax kernel
float nvidia_softmax_topK_online(
  float **values,
  int **index,
  float **probabilities,
  int num_elements,
  int top_k,
  int batch_size,
  float * x_host) {
  float TopKTime = 0;
  cudaEvent_t startTopK;
  cudaEvent_t stopTopK;
  CUDA_CHECK(cudaEventCreate(&startTopK));
  CUDA_CHECK(cudaEventCreate(&stopTopK));

  CUDA_CHECK(cudaEventRecord(startTopK));
  online_softmax_topk<MAX_K,256><<<batch_size,256>>>(*values, *index, *probabilities, num_elements, top_k);

  CUDA_CHECK(cudaEventRecord(stopTopK));
  CUDA_CHECK(cudaEventSynchronize(stopTopK));
  CUDA_CHECK(cudaEventElapsedTime(&TopKTime, startTopK, stopTopK));


  CUDA_CHECK(cudaMemcpy(x_host,*probabilities,(size_t)top_k * batch_size * sizeof(float),cudaMemcpyDeviceToHost));
  return TopKTime;
}
